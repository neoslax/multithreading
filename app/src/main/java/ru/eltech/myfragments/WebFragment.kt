package ru.eltech.myfragments

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import ru.eltech.myfragments.databinding.FragmentWebBinding

class WebFragment : Fragment(), OnBackPressedWebListener {

    private var _binding: FragmentWebBinding? = null
    private val binding: FragmentWebBinding
        get() = _binding ?: throw RuntimeException("FragmentWebBinding == null")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWebBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding.wvWeb) {
            webViewClient = WebViewClient()
            this.apply {
                loadUrl("https://www.google.ru/")
                settings.javaScriptEnabled = true
            }
        }
    }

    override fun onBackPressed(): Boolean {
        return if (binding.wvWeb.canGoBack()) {
            //action not popBackStack
            binding.wvWeb.goBack()
            true
        } else {
            false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

interface OnBackPressedWebListener {
    fun onBackPressed(): Boolean
}