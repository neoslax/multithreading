package ru.eltech.myfragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import ru.eltech.myfragments.databinding.FragmentRegistrationBinding

class RegistrationFragment : Fragment() {
    private var _binding: FragmentRegistrationBinding? = null
    private val binding: FragmentRegistrationBinding
        get() = _binding ?: throw RuntimeException("FragmentRegistrationBinding == null")

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegistrationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnRegProceed.setOnClickListener {
            showProgressBar()
            val email = binding.etRegEmail.text.toString().trim()
            val password = binding.etRegPassword1.text.toString()
            val name = binding.etRegName.text.toString()
            val phone = binding.etRegPhoneNumber.text.toString()
            if (validateInput()) {
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener {
                        if (it.isSuccessful) {

                            val userInfo = UserInfoModel(
                                email,
                                name,
                                phone
                            )
                            auth.uid?.let { uid ->
                                FirebaseDatabase.getInstance().getReference("Users")
                                    .child(uid)
                                    .setValue(userInfo)
                                    .addOnCompleteListener {
                                        if (it.isSuccessful) {
                                            hideProgressBar()
                                            Toast.makeText(
                                                requireContext(),
                                                "Success",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                            findNavController().navigate(R.id.action_registrationFragment_to_loginFragment)
                                            Log.d("RegistrationFragment", it.result.toString())
                                        }
                                    }

                            }


                        } else {
                            hideProgressBar()
                            Toast.makeText(requireContext(), "Error see logs", Toast.LENGTH_SHORT)
                                .show()
                            Log.d("RegistrationFragment", it.exception.toString())
                        }
                    }

            } else {
                hideProgressBar()
                Toast.makeText(requireContext(), "Incorrect input", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun validateInput(): Boolean {
        val email = binding.etRegEmail.text.toString().trim()
        val password = binding.etRegPassword1.text.toString()
        val name = binding.etRegName.text.toString()
        val phone = binding.etRegPhoneNumber.text.toString()
        var result = true

        if (email.isEmpty()) {
            binding.etRegEmail.error = "Error"
            result = false
        }
        if (password.isEmpty() || password.length < 8) {
            if (password.length < 8) {
                binding.etRegPassword1.error = "Password too short"
            }
            binding.etRegPassword1.error = "Error"
            result = false
        }
        if (name.isEmpty()) {
            binding.etRegName.error = "Error"
            result = false
        }
        if (phone.isEmpty()) {
            binding.etRegPhoneNumber.error = "Error"
            result = false
        }

        return result
    }

    private fun showProgressBar() {
        binding.pbReg.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.pbReg.visibility = View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}