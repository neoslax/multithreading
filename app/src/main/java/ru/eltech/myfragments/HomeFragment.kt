package ru.eltech.myfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ru.eltech.myfragments.adapter.Item
import ru.eltech.myfragments.adapter.ItemAdapter
import ru.eltech.myfragments.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding
        get() = _binding ?: throw RuntimeException("FragmentHomeBinding == null")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()

    }

    private fun setupRecyclerView() {
        val adapter = ItemAdapter()
        binding.rvHome.adapter = adapter
        adapter.submitList(getTestList())

    }

    private fun getTestList(): List<Item> {
        val result = mutableListOf<Item>()
        (0..10).forEach { result.add(Item("Element $it", it)) }
        return result
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}