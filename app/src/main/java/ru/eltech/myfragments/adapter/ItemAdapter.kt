package ru.eltech.myfragments.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.ListAdapter
import kotlinx.coroutines.*
import ru.eltech.myfragments.R
import ru.eltech.myfragments.databinding.ItemBinding

class ItemAdapter : ListAdapter<Item, ItemVH>(ItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemVH {
        val binding = ItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ItemVH(binding)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ItemVH, position: Int) {
        val item = getItem(position)
        holder.binding.tvItem.text = item.name
        holder.binding.tvCount.text = "${item.count}"

        val styleList = listOf(R.style.Style1, R.style.Style2, R.style.Style3)

        val scope = CoroutineScope(Dispatchers.Main)
        scope.launch {
            while (true) {
                for (element in styleList) {
                    holder.binding.tvItem.setTextAppearance(element)
                    holder.binding.tvCount.setTextAppearance(element)
                    delay(1000L)
                }
            }
        }


    }
}