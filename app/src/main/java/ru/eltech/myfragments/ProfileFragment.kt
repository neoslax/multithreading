package ru.eltech.myfragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ru.eltech.myfragments.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {

    private lateinit var bottomBarEnabler: LoginFragment.BottomBarEnabler

    private var _binding: FragmentProfileBinding? = null
    private val binding: FragmentProfileBinding
        get() = _binding ?: throw RuntimeException("FragmentProfileBinding == null")

    private lateinit var auth: FirebaseAuth

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is LoginFragment.BottomBarEnabler) {
            bottomBarEnabler = context
        } else throw RuntimeException("Activity must implement BottomBarEnabler")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        if (auth.currentUser == null) {
            findNavController().navigate(R.id.action_profileFragment2_to_loginFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProfileData()
        bottomBarEnabler.setBottomBarVisible()
        binding.btnProfileLogout.setOnClickListener {
            logout()
            findNavController().navigate(R.id.action_profileFragment2_to_loginFragment)
        }
    }

    private fun logout() {
        auth.signOut()

    }

    private fun setProfileData() {
        val user = auth.currentUser
        val reference = FirebaseDatabase.getInstance().getReference("Users")
        val userID = user?.uid
        userID?.let {
            reference.child(userID).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    val userInfo = p0.getValue(UserInfoModel::class.java)
                    userInfo?.let {
                        Log.d("ProfileFragment", "$it")
                        binding.tvProfileEmail.text = it.email
                        binding.tvProfileName.text = it.name
                        binding.tvProfilePhone.text = it.phone
                    }

                }

                override fun onCancelled(p0: DatabaseError) {
                    Log.d("ProfileFragment", "canceled")
                }
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}