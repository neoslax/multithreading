package ru.eltech.myfragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ru.eltech.myfragments.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private lateinit var bottomBarEnabler: BottomBarEnabler
    private lateinit var auth: FirebaseAuth

    private var _binding: FragmentLoginBinding? = null
    private val binding: FragmentLoginBinding
        get() = _binding ?: throw RuntimeException("FragmentLoginBinding == null")

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BottomBarEnabler) {
            bottomBarEnabler = context
        } else throw RuntimeException("Activity must implement BottomBarEnabler")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null) {
            findNavController().navigate(R.id.action_loginFragment_to_profileFragment2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvLoginToRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registrationFragment)
        }
        binding.btnLogin.setOnClickListener {
            //findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
            if (isInputCorrect()) {

                login()
            }
        }


    }

    private fun login() {

        val email = binding.etLoginUsername.text.toString().trim()
        val pass = binding.etLoginPassword.text.toString()

        auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener {
            if (it.isSuccessful) {
                Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()
                bottomBarEnabler.setBottomBarVisible()
                val user = auth.currentUser
                val reference = FirebaseDatabase.getInstance().getReference("Users")
                val userID = user?.uid
                userID?.let {
                    reference.child(userID)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(p0: DataSnapshot) {
                                val userInfo = p0.getValue(UserInfoModel::class.java)
                                userInfo?.let {
                                    Log.d("LoginFragment", "$it")
                                }

                            }

                            override fun onCancelled(p0: DatabaseError) {
                                Log.d("LoginFragment", "canceled")
                            }
                        })
                }
                findNavController().navigate(R.id.action_loginFragment_to_profileFragment2)

            } else {
                Toast.makeText(
                    requireContext(),
                    "Username/Password is incorrect",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }

    private fun isInputCorrect(): Boolean {
        val email = binding.etLoginUsername.text.toString().trim()
        val pass = binding.etLoginPassword.text.toString()

        if (email.isEmpty()) {
            binding.etLoginUsername.error = "Error"
        }
        if (pass.isEmpty()) {
            binding.etLoginPassword.error = "Error"
        }
        if (email.isEmpty() || pass.isEmpty()) return false
        return true
    }

    interface BottomBarEnabler {
        fun setBottomBarVisible()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}