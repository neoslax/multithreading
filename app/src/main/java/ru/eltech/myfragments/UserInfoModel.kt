package ru.eltech.myfragments

data class UserInfoModel(
    val email: String = "",
    val name: String = "",
    val phone: String = ""
)
